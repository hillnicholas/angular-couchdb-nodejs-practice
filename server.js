
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var apiRoutes = require('./api');

var app = express();

app.use( 
  bodyParser.json( {limit : '50mb' } ) 
);

// set static frontend
app.use( 
  express.static( 'public' )
 );

// use the API routes
apiRoutes( app );

console.log("Listening on 8080...");
app.listen(8080);


