var MongoClient = require("mongodb").MongoClient;
//var databaseName = "testdb";
//var url = "mongodb://localhost/" + databaseName;

module.exports.getResults = 
    function (req, res) {
    //console.log(req.query);
    MongoClient.connect('mongodb://localhost:27017/', function(err, db) {
        if ( err ) throw err;
        var collection = db.db("testdb").collection('testcol1');
        
        // its not perfect... 
        if ("age" in req.query) req.query["age"] = parseInt( req.query["age"]);

        console.log("Received: " + JSON.stringify( req.query ) );
        collection.find(req.query).toArray( function( err, arr ) {
                    arr.map( row => delete row._id );
                    res.json( arr );
            });
    });
}


module.exports.getResultsTwitter = 
    function (req, res) {
    //console.log(req.query);
    MongoClient.connect('mongodb://localhost:27017/', function(err, db) {
        if ( err ) throw err;
        var collection = db.db("testdb").collection('tweets');
        
        // its not perfect... 
        //if ("age" in req.query) req.query["age"] = parseInt( req.query["age"]);

        console.log("Received: " + JSON.stringify( req.query ) );
        collection.find(req.query).toArray( function( err, arr ) {
                    arr.map( row => delete row._id );
                    res.json( arr );
            });
    });
}
