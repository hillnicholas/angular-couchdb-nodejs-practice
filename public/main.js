var main = angular.module("main", [] );



main.controller("mainController", function( $scope, $http ) {
    $scope.dbpath = "/test1";
    $scope.defaultQuery = {};
    $scope.customQueryButtonText = "custom";
    $scope.isCustomQueryOpen = false;
    $scope.customQueryParams = {};
    $scope.isLoading = false;
    $scope.getQuery = function( query ) {

        var data;
        var arr = Array();
        for( key in query ) {
            if( query[key]) {
                arr.push( key + "=" + query[key] );
            }
        }
        var serialized = arr.join("&");
        $scope.isLoading = true;
        fetch( $scope.dbpath + "?" + serialized )
        .then(
            response => response.json()
         )
        .then( response => {
                    $scope.data = response;
                    $scope.isLoading = false;
                    if (! $scope.fields ) {
                        $scope.fields = Object.keys($scope.data[0]);
                    }
                    $scope.$apply();
            }
        );
    }

    $scope.debug = function() {
        console.log("test");
        for( key in $scope.customQueryParams ) {
            console.log( key + " : " + $scope.customQueryParams[ key ] );
        }
    }

    $scope.toggleCustom = function() {
        // do actions based on if query is open
        if ($scope.isCustomQueryOpen) {
            $scope.getQuery( $scope.customQuery );
        }
        // toggle if query is open
        $scope.isCustomQueryOpen = ! $scope.isCustomQueryOpen;
        // do actions with updated state
        if ($scope.isCustomQueryOpen) {
            $scope.customQueryButtonText = "Run query";
        }
        else {
            $scope.customQueryButtonText = "custom";
        }
    }

    $scope.init = function() {
        console.log("init");
        $scope.getQuery( $scope.defaultQuery );

    }
});